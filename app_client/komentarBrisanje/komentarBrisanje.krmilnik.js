(function() {
  /* global angular */
  
  function komentarModalnoOkno($uibModalInstance, edugeocachePodatki, podrobnostiLokacije) {
    var vm = this;
    
    vm.podrobnostiLokacije = podrobnostiLokacije;
    
    vm.modalnoOkno = {
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      },
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    //Spremenimo vrednosti za dodajanje komentarja, kot parameter potrebujemo še idKomentarja
    vm.izbrisiKomentar = function(idLokacije, idKomentarja) {
      edugeocachePodatki.izbrisiKomentarZaId(idLokacije, idKomentarja).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri izbrisu komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      //po potrebi spremenimo preverjanje..

        //dodamo idKomentarja
        vm.izbrisiKomentar(vm.podrobnostiLokacije.idLokacije, vm.podrobnostiLokacije.idKomentarja);
    };
  };
  komentarModalnoOkno.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije'];
  
  angular
    .module('edugeocache')
    .controller('komentarBrisanje', komentarModalnoOkno);
})();