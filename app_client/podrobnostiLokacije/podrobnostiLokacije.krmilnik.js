(function() {
  /* global angular */
  
  function podrobnostiLokacijeCtrl($routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija) {
    var vm = this;
    vm.idLokacije = $routeParams.idLokacije;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    vm.uporabnik = avtentikacija.trenutniUporabnik();
    
    vm.prvotnaStran = $location.path();
    vm.vsebinaKomentarja = "";
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
    
    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
    //
    //Prikaze se pojavno okno za urejanje komentarja. Za nov kontroler je potrebno v app.js nastavit pot!
    //
    vm.prikaziUrejanje = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarUrejanje/komentarUredi.pogled.html',
        controller: 'komentarUrejanje',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv,
              besedilo: vm.vsebinaKomentarja.besediloKomentarja,
              idKomentarja: vm.vsebinaKomentarja._id
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        console.log(podatki);
        for (var i = 0; i < vm.podatki.lokacija.komentarji.length; i++) {
          console.log(vm.podatki.lokacija.komentarji[i]._id + "\n" + podatki._id);
          if (vm.podatki.lokacija.komentarji[i]._id == podatki._id) {
            vm.podatki.lokacija.komentarji[i] = podatki;
          }
        }
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
     //
    //Prikaze se pojavno okno za brisanje komentarja!!
    //
    vm.prikaziBrisanje = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarBrisanje/komentarBrisanje.pogled.html',
        controller: 'komentarBrisanje',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              idKomentarja: vm.vsebinaKomentarja._id
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        var novaTabela = [];
        for (var i = 0; i < vm.podatki.lokacija.komentarji.length; i++) {
          if (vm.podatki.lokacija.komentarji[i]._id != vm.vsebinaKomentarja._id) {
           novaTabela.push(vm.podatki.lokacija.komentarji[i]);
          }
        }
        vm.podatki.lokacija.komentarji = novaTabela;
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };   
  }
  podrobnostiLokacijeCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija'];
  
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();