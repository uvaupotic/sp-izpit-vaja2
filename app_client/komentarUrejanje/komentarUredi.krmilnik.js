(function() {
  /* global angular */
  
  function komentarModalnoOkno($uibModalInstance, edugeocachePodatki, podrobnostiLokacije) {
    var vm = this;
    
    vm.podrobnostiLokacije = podrobnostiLokacije;
    
    vm.modalnoOkno = {
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      },
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    //Spremenimo vrednosti za dodajanje komentarja, kot parameter potrebujemo še idKomentarja
    vm.dodajKomentar = function(idLokacije, idKomentarja, podatkiObrazca) {
      edugeocachePodatki.posodobiKomentarZaId(idLokacije, idKomentarja, {
        naziv: podatkiObrazca.naziv,
        ocena: podatkiObrazca.ocena,
        komentar: vm.podrobnostiLokacije.besedilo
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      //po potrebi spremenimo preverjanje...
      if (!vm.podatkiObrazca.ocena || !vm.podrobnostiLokacije.besedilo) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja sem tu not!";
        return false;
      } else {
        //dodamo idKomentarja
        vm.dodajKomentar(vm.podrobnostiLokacije.idLokacije, vm.podrobnostiLokacije.idKomentarja, vm.podatkiObrazca);
      }
    };
  };
  komentarModalnoOkno.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije'];
  
  angular
    .module('edugeocache')
    .controller('komentarUrejanje', komentarModalnoOkno);
})();