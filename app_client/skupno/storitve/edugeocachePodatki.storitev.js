(function() {
  /* global angular */
  
  var edugeocachePodatki = function($http, avtentikacija) {
    var koordinateTrenutneLokacije = function(lat, lng) {
      return $http.get('/api/lokacije?lng=' + lng + '&lat=' + lat + '&maxRazdalja=100');
    };
    var podrobnostiLokacijeZaId = function(idLokacije) {
      return $http.get('/api/lokacije/' + idLokacije);
    };
    var dodajKomentarZaId = function(idLokacije, podatki) {
      return $http.post('/api/lokacije/' + idLokacije + '/komentarji', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    //Dodamo novo funkcijo za posodabljanje komentarja in jo ustrezno poimenujemo
    var posodobiKomentarZaId = function(idLokacije, idKomentarja, podatki) {
      return $http.post('/api/lokacije/' + idLokacije + '/komentarjiUredi' + idKomentarja, podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var izbrisiKomentarZaId = function(idLokacije, idKomentarja) {
      return $http.delete('/api/lokacije/' + idLokacije + '/komentarjiIzbrisi' + idKomentarja,  {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    return {
      koordinateTrenutneLokacije: koordinateTrenutneLokacije,
      podrobnostiLokacijeZaId: podrobnostiLokacijeZaId,
      dodajKomentarZaId: dodajKomentarZaId,
      posodobiKomentarZaId: posodobiKomentarZaId,
      izbrisiKomentarZaId: izbrisiKomentarZaId
    };
  };
  edugeocachePodatki.$inject = ['$http', 'avtentikacija'];
  
  angular
    .module('edugeocache')
    .service('edugeocachePodatki', edugeocachePodatki);
})();